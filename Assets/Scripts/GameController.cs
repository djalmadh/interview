﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GameController : MonoBehaviour
{
    public GameObject trunk; 
    public Transform spawnPoint;
    public int maxSize;
    private Vector3 top, initialPos;
    private Coroutine co;
    private int count;

    public int TrunkAmount { private set; get; }

    private void Start()
    {
       initialPos = spawnPoint.position;
       top = new Vector3(0,trunk.GetComponent<MeshFilter>().sharedMesh.bounds.extents.y*1.82f,0);

        PlaySpawn();
    }
    //Function that starts the corroutine responsable for the Spawn of the trunk
    public void PlaySpawn()
    {
        if(co == null)
        {
           co = StartCoroutine(Spawn());
        }
       
    }
    //Corroutine Responsible for the spawning and position adjustment of the trunks
    private IEnumerator Spawn()
    {
        count = 0;
        spawnPoint.GetComponent<PlayableDirector>().enabled = false;
        spawnPoint.position = initialPos;

        var trunks = GameObject.FindGameObjectsWithTag("Trunk");
        foreach(GameObject a in trunks)
        {
            Destroy(a);
        }

        TrunkAmount = Random.Range(3, maxSize);
        Vector3 pos = Vector3.zero;

        for (int i = 0; i < TrunkAmount; i ++) 
        {
            yield return new WaitForSeconds(0.025f);
            if (i != 0)
            {
                var obj = Instantiate(trunk , spawnPoint);
                pos += top;
                obj.transform.position = pos;
            }
            else
            {
                var obj = Instantiate(trunk , spawnPoint);
                pos = obj.transform.position;
            }
        }

        spawnPoint.GetComponent<PlayableDirector>().enabled = true;
        yield return new WaitForSeconds(.65f);
        StopCoroutine(co);
        co = null;
    }

    //Function that starts the corroutine responsable for the Destruction of the trunk

    public void PlayDestroy()
    {
        if (co == null) {
            StartCoroutine(DestroyTrunkCorroutine());
        }
    }

    //This corroutine Identifies the trunk that will be destroyed and calls the functions from the DestroyedTrunck class that are
    //Responsable for the Destruction and Movement of the Trunk

    private IEnumerator DestroyTrunkCorroutine()
    {
        if (spawnPoint.childCount > count)
        { 
            if (spawnPoint.GetChild(count) && spawnPoint.GetChild(count).GetComponent<DestroyedTrunk>())
            {
                spawnPoint.GetChild(count).GetComponent<DestroyedTrunk>().DestroyTrunk();
                count++;
                yield return new WaitForSeconds(.05f);
                for (int i = count; i < spawnPoint.childCount; i++)
                {
                    spawnPoint.GetChild(i).GetComponent<DestroyedTrunk>().MoveTrunk(top, 20f);
                }
                if (spawnPoint.childCount == count)
                {
                    yield return new WaitForSeconds(1f);
                    PlaySpawn();
                }
            }
        }
    }
}
