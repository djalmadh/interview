﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedTrunk : MonoBehaviour
{
    private bool moveNeeded = false;
    private float t = 0, lerpTime = 0;
    private Vector3 movePos;

    //Lerps the position of the game object this script is attached to after the MoveTrunck function is called 

    private void Update()
    {

        if(moveNeeded == true)
        {
            transform.position = Vector3.Lerp(transform.position, movePos, lerpTime * Time.deltaTime);
            t = Mathf.Lerp(t, 1f, lerpTime * Time.deltaTime);
        }
        
    }

    //Function responsible for disabling the renderer and enabling the particle system for each trunck

    public void DestroyTrunk()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        transform.Find("Particles").gameObject.SetActive(true);
    }

    //Function responsible for setting up the movement for each trunk;

    public void MoveTrunk(Vector3 pos, float a)
    {
        t = 0;
        moveNeeded = true;
        movePos = transform.position - pos;
        lerpTime = a;
    }
}
